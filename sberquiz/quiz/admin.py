"""admin for quizzes"""
from django.contrib import admin
from quiz.models import Quiz, Question, Answer


class QuestionsInline(admin.TabularInline):
    """item model admin inline"""
    model = Question
    extra = 1


class AnswersInline(admin.TabularInline):
    """item model admin inline"""
    model = Answer
    extra = 1


class QuizAdmin(admin.ModelAdmin):
    """admin view for quiz"""
    inlines = [QuestionsInline, ]


class QuestionAdmin(admin.ModelAdmin):
    """admin view for question"""
    inlines = [AnswersInline, ]


admin.site.register(Quiz, QuizAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Answer)
