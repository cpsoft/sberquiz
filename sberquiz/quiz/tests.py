"""test app"""
from django.test import TestCase
from quiz.models import Quiz, Question, Answer


class SberTestCase(TestCase):
    """test sber quiz app"""
    fixtures = ['initial_data.json']

    def test_quiz(self):
        """test quiz model"""
        self.assertGreaterEqual(Quiz.objects.count(), 1, "At least one quiz")
        for _quiz in Quiz.objects.all():
            questions = Question.objects.filter(quiz=_quiz)
            self.assertGreaterEqual(questions.count(), 1, "At least one question per quiz %s" % _quiz)
        _q = Quiz.objects.first()
        self.assertEqual(_q.finished, 0, "Quiz finished zero times at start")
        _q.finish()
        self.assertEqual(_q.finished, 1, "Quiz finished once after finish()")

    def test_question(self):
        """test question model"""
        self.assertGreaterEqual(Question.objects.count(), 1)
        for _question in Question.objects.all():
            _answers = Answer.objects.filter(question=_question)
            self.assertGreaterEqual(_answers.count(), 2, "At least one answer per question %s" % _question)
            _trueanswers = Answer.objects.filter(question=_question, is_correct=True)
            self.assertEqual(_trueanswers.count(), 1, "Only one correct answer per question %s" % _question)
            _falseanswers = Answer.objects.filter(question=_question, is_correct=False)
            self.assertGreaterEqual(_falseanswers.count(), 1, "At least one incorrect answer per question %s" % _question)

    def test_answer(self):
        """test answer model"""
        self.assertGreaterEqual(Answer.objects.count(), 1, "At least one answer")
        for _a in Answer.objects.all():
            self.assertGreaterEqual(len(str(_a)), 1, "Some text in answer")

    def test_client(self):
        """test http queries"""
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/q/beaver')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/q/100500')
        self.assertEqual(response.status_code, 404)
        response = self.client.get('/notfound')
        self.assertEqual(response.status_code, 404)
