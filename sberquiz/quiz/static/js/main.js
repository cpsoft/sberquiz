$(document).ready(function(){
    $('.owl-carousel').owlCarousel({
        items:1,
        loop: false
    });

    getSquirrel();
    getBird();
    getBeaver();
});

function getSquirrel() {
    var squirrel = Cookies.get('squirrel');
    if(squirrel) {
        $('.saved.squirrel').show();
        $('.your.squirrel').html(squirrel);
        if($('.count.squirrel').html() > 0) {
            $('.progress.squirrel').css('width', ((squirrel/$('.count.squirrel').html())*100) + '%');
        }
    } else {
        $('.test.squirrel').show();
    }
}

function getBird() {
    var bird = Cookies.get('bird');
    if(bird) {
        $('.saved.bird').show();
        $('.your.bird').html(bird);
        if($('.count.bird').html() > 0) {
            $('.progress.bird').css('width', ((bird/$('.count.bird').html())*100) + '%');
        }
    } else {
        $('.test.bird').show();
    }
}

function getBeaver() {
    var beaver = Cookies.get('beaver');
    if(beaver) {
        $('.saved.beaver').show();
        $('.your.beaver').html(beaver);
        if($('.count.beaver').html() > 0) {
            $('.progress.beaver').css('width', ((beaver/$('.count.beaver').html())*100) + '%');
        }
    } else {
        $('.test.beaver').show();
    }
}