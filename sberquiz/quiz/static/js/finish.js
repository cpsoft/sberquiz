$(document).ready(function(){
    updateCounter();
});

function updateCounter() {
    var $result = $('.text-result');
    var animal = $result.data('animal');
    var count = Cookies.get(animal);
    count = count ? count : 0;
    $result.html(count);
}