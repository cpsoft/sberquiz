"""quiz views"""
from django.http import HttpResponse, Http404
# from django.views import View
from django.template import loader
from quiz.models import Quiz, Question, Answer


def home(request):
    """home page view"""
    template = loader.get_template('main.html')
    beavers = Quiz.objects.filter(slug='beaver')
    birds = Quiz.objects.filter(slug='bird')
    squirrels = Quiz.objects.filter(slug='squirrel')
    context = {
        'squirrels': squirrels.first(),
        'beavers': beavers.first(),
        'birds': birds.first(),
    }
    return HttpResponse(template.render(context, request))


def quiz_start(request, quiz_slug):
    """start quiz page view"""
    template = loader.get_template('quiz_start.html')
    try:
        quiz_ = Quiz.objects.get(slug=quiz_slug)
    except Quiz.DoesNotExist:
        raise Http404("Quiz #%s does not exist" % (quiz_slug))
    questions = Question.objects.filter(quiz=quiz_).order_by("order")
    context = {
        'quiz': quiz_,
        'questions': questions,
    }
    return HttpResponse(template.render(context, request))


def quiz_go(request, quiz_slug, question_num=1):
    """start quiz page view"""
    try:
        quiz_ = Quiz.objects.get(slug=quiz_slug)
    except Quiz.DoesNotExist:
        raise Http404("Quiz #%s does not exist" % (quiz_slug))
    question = Question.objects.filter(quiz=quiz_, order=question_num).first()
    answers = Answer.objects.filter(question=question)
    if question is None:
        template = loader.get_template('quiz_finish.html')
        _next = "/questions/%s/%s" % (quiz_.slug, 999)
    else:
        template = loader.get_template('quiz_go.html')
        _next = "/questions/%s/%s" % (quiz_.slug, question_num + 1)

    context = {
        'quest_current': question_num,
        'quest_all': Question.objects.filter(quiz=quiz_).count(),
        'quiz': quiz_,
        'question': question,
        'answers': answers,
        'next': _next
    }
    return HttpResponse(template.render(context, request))

def quiz_add(request, quiz_slug):
    """start quiz page view"""
    try:
        quiz_ = Quiz.objects.get(slug=quiz_slug)
    except Quiz.DoesNotExist:
        raise Http404("Quiz #%s does not exist" % (quiz_slug))
    print(quiz_)
    quiz_.finish()
    return HttpResponse('')
