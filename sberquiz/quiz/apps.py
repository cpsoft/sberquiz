"""quiz app"""

from django.apps import AppConfig


class QuizConfig(AppConfig):
    """quiz config"""
    name = 'quiz'
