"""models for quiz app"""
from django.db import models


class Quiz(models.Model):
    """Represesns quiz - a set of questions"""
    title = models.CharField(max_length=100)
    slug = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    finished = models.IntegerField()

    def finish(self):
        """Call when test is finished"""
        self.finished = self.finished + 1
        self.save()

    def __str__(self):
        return self.title

    class Meta:
        """Meta class"""
        # pylint: disable=too-few-public-methods
        verbose_name_plural = "Quizzes"


class Question(models.Model):
    """Represents Question in a quiz"""
    order = models.IntegerField()
    correct_ans = models.CharField(max_length=2)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
    description = models.CharField(max_length=300)

    def __str__(self):
        return self.description


class Answer(models.Model):
    """Represents a question for an answer"""
    slug = models.CharField(max_length=2)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    description = models.CharField(max_length=100)
    hint = models.CharField(max_length=100)

    def __str__(self):
        return self.description
